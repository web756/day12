<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <script src="https://cdn.tailwindcss.com"></script>

    <link rel="stylesheet" href="./style/global.css">
</head>
<body>
    <?php
        session_start();
        include ('./connection.php');
        $name = $_SESSION['registry_data']['fullName'];
        $gender = $_SESSION['registry_data']['gender'];
        $fac  = $_SESSION['registry_data']['faculty'];
        $birthday = date("Y-m-d", strtotime(implode("-", explode("/", $_SESSION['registry_data']['birthday']))));
        $address = $_SESSION['registry_data']['address'];
        if(isset($_SESSION['registry_data']['uploadedImage']))
            $avartar = $_SESSION['registry_data']['uploadedImage'];
        else {
            $avartar = '';
        }
        $sql = "INSERT INTO `student` (`name`, `gender`, `faculty`, `birthday`, `address`, `avartar`) VALUES ('$name', '$gender', '$fac', '$birthday', '$address', '$avartar')";
        $connection -> exec($sql);
    ?>

    <div class="">
        <div class="">
            <div class="">
                <p class="mt-12 text-center">Bạn đã đăng ký thành công sinh viên</p>
                <p class="text-center">
                    <a class="text-center underline decoration-sky-600 md:decoration-blue-400" href="./index.php">Quay lại danh sách sinh viên</a>
                </p>
            </div>
        </div>
    </div>
</body>
</style>
</html>