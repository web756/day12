<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
</head>
<body>
    <?php 
        $departments = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
        // Get data student từ database 
        include ('./connection.php');
        $getData = "SELECT * FROM `student`";
        $getQuantity = "SELECT COUNT(`student`.id) AS COUNT FROM `student`";
        $count = $connection -> query($getQuantity);
        $datas = $connection -> query($getData);    

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $_COOKIE["department"] = $_POST["department"];
            $_COOKIE["keyword"] = $_POST["keyword"];
        }
    ?>


    <div class="my-container py-6 px-12">
        <form action="" method="POST" enctype="multipart/form-data">

            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label 
                        class="
                            block
                            p-2"
                        for="faculty"
                    >
                        Khoa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="h-full">
                        <select 
                            class="w-3/4 h-full ml-10 border-solid border-2 border-[#4f85b4]" 
                            name="faculty" 
                            id="faculty"
                            >
                                <?php
                                    $faculty = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                                    foreach ($faculty as $key => $value) {
                                        echo "<option value='$key'>$value</option>";
                                    }
                                ?>
                        </select>
                        <div class="triangle inline"></div>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label 
                        class="
                            block 
                            p-2" 
                        for="keyword"
                    >
                        Từ khóa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <input 
                        class="
                            w-3/4
                            h-full
                            ml-10
                            border-solid
                            border-2
                            border-[#4f85b4]"
                        type="text"
                        id="keyword"
                        name="keyword"
                    >
                </div>
            </div>

            <div class="flex mt-3">
                <div class="w-1/4"></div>
                <div class="w-3/4 float-right">
                    <button 
                        name="delete-button" 
                        id="delete-button" 
                        class="relative 
                            left-[23%]
                            w-1/4 
                            cursor-pointer 
                            p-2 
                            rounded-lg 
                            border-solid 
                            border-2 
                            bg-[#4F81BD] 
                            border-[#4f85b4] 
                            text-white"
                    >   
                        Xóa
                    </button>
                    <input 
                        class="
                            relative 
                            left-[23%]
                            w-1/4 
                            cursor-pointer 
                            p-2 
                            rounded-lg 
                            border-solid 
                            border-2 
                            bg-[#4F81BD] 
                            border-[#4f85b4] 
                            text-white" 
                        type="submit" 
                        value="Tìm kiếm"
                        name="submit"
                    >
                </div>

            </div>
            

        </form>

    </div>

    <div class="wrapper">
        <div class="flex justify-between items-center mx-auto mt-4">
            <div class="students-found">
                <span>Số sinh viên tìm thấy:</span>
                <span>
                    <?php while($quantity = $count -> fetch(PDO::FETCH_ASSOC)) {
                            echo $quantity["COUNT"];
                        }?>
                </span>
            </div>
            <div class="add-button">
                <form action="register.php">
                    <input 
                        type="submit" 
                        class="
                            cursor-pointer 
                            p-2 
                            rounded-lg 
                            border-solid 
                            border-2 
                            bg-[#4F81BD] 
                            border-[#4f85b4] 
                            text-white
                            w-[100px]
                            mr-[90px]" 
                        value="Thêm">
                </form>
            </div>
        </div>
    
        <div class="mt-4">
            <table class="w-full">
                <thead>
                    <tr>
                        <th class="text-left font-normal">No</th>
                        <th class="text-left font-normal">Tên sinh viên</th>
                        <th class="text-left font-normal">Khoa</th>
                        <th class="text-left font-normal">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $order = 0;
                        while ($data = $datas -> fetch(PDO::FETCH_ASSOC)) {
                            $order++;
                            echo '<tr>
                                <td class="text-center">'.$order.'</td>
                                <td>'.$data['name'].'</td>
                                <td>'.$departments[$data['faculty']].'</td>
                                <td class="action-table">
                                    <input 
                                        type="submit" 
                                        class="
                                        cursor-pointer 
                                        px-3
                                        py-1 
                                        mt-2
                                        border-solid 
                                        border-2 
                                        bg-[#8BAACF] 
                                        border-[#4f85b4] 
                                        text-white" 
                                    value="Xóa"
                                    >
                                    <input 
                                        type="submit" 
                                        class="
                                            cursor-pointer 
                                            px-3
                                            py-1 
                                            border-solid 
                                            border-2 
                                            bg-[#8BAACF] 
                                            border-[#4f85b4] 
                                            text-white" 
                                        value="Sửa"
                                    >
                                </td>
                            </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>  
    </div>

</body>

<script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        document.querySelector("#delete-button").addEventListener("click", (e) => {
                $('#faculty').val(0);
                $('#keyword').val("");

                e.preventDefault()
        })
        
    })
    $("#faculty").val(decodeURI(getCookie("faculty")));
    if (typeof getCookie("keyword") !== 'undefined')
        $("#keyword").val(decodeURI(getCookie("keyword")));

    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }
</script>

</html>